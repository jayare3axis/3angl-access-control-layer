<?php

namespace Triangl\Entity;

/**
 * Entity with reference to domain where it belongs.
 */
interface BelongsToDomainInterface {
    /**
     * Gets the domain.
     * @return \Triangl\Entity\Security\Domain
     */
    public function getDomain();
}
