<?php

namespace Triangl\Entity;

/**
 * Entity with reference to domain where it belongs.
 */
trait BelongsToDomainTrait {
    /**
     * @ManyToOne(targetEntity="\Triangl\Entity\Security\Domain")
     * @JoinColumn(name="domain_id", referencedColumnName="id")
     **/
    private $domain;
    
    /**
     * Gets the domain.
     * @return \Triangl\Entity\ContentManagementSystem\Domain
     */
    public function getDomain()
    {
        return $this->domain;
    }
}
