<?php

namespace Triangl\Entity;

use Doctrine\ORM\Mapping\ClassMetaData;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * Filtres entity by domain where it belongs.
 */
class DomainFilter extends SQLFilter {
    /**
     * Implemented.
     */
    public function addFilterConstraint(ClassMetaData $targetEntity, $targetTableAlias)
    {
        if ( !$targetEntity->reflClass->implementsInterface('\Triangl\Entity\BelongsToDomainInterface') ) {            
            return '';
        }        
        return $targetTableAlias.'.domain_id = ' . $this->getParameter('domain_id');
    }
}
