<?php

namespace Triangl\Entity\Security;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\NameTrait;
use Triangl\Entity\AliasTrait;

/**
 * Action entity.
 * @Entity @Table(name="actions")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 **/
class Action {
    use PrimaryIdTrait;    
    use NameTrait;
    use AliasTrait;
    
    /** @Column(type="string") **/
    private $protected;
    
    /**
     * Tells if action is protected.
     * @return boolean
     */
    public function isProtected() {
        return $this->protected;
    }

    /**
     * Sets protected value.
     * @param boolean $protected
     */
    public function setProtected($protected = true) {
        $this->protected = $protected;
    }
}
