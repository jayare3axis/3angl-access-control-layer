<?php

namespace Triangl\Entity\Security;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\NameTrait;
use Triangl\Entity\Security\UserDomainAssociationTrait;
use Triangl\Entity\AliasTrait;

/**
 * Domain entity.
 * @Entity @Table(name="domains")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 **/
class Domain {
    use PrimaryIdTrait;    
    use NameTrait;
    use UserDomainAssociationTrait;
    use AliasTrait;
    
    /**
     * @OneToMany(targetEntity="\Triangl\Entity\Security\UserDomainAssociation", mappedBy="domain")
     */
    private $user_domain_associations;
    
    /**
     * Default constructor.
     */
    public function __construct() {
        $this->user_domain_associations = new ArrayCollection();
    }
}
