<?php

namespace Triangl\Entity\Security;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\NameTrait;
use Triangl\Entity\Security\Action;
use Triangl\Entity\Security\RoleFirewallAssociationTrait;

/**
 * Firewall entity.
 * @Entity @Table(name="firewalls")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 **/
class Firewall {
    use PrimaryIdTrait;    
    use NameTrait;
    use RoleFirewallAssociationTrait;
    
    /** @Column(type="string") **/
    private $resource;
    
    /**
     * @ManyToOne(targetEntity="\Triangl\Entity\Security\Action")
     * @JoinColumn(name="action_id", referencedColumnName="id")
     **/
    private $action;
    
    /**
     * @OneToMany(targetEntity="\Triangl\Entity\Security\RoleFirewallAssociation", mappedBy="firewall")
     */
    private $role_firewall_associations;
    
    /**
     * Default constructor.
     */
    public function __construct() {
        $this->role_firewall_associations = new ArrayCollection();
    }
    
    /**
     * Gets the resource.
     * @return string
     */
    public function getResource() {
        return $this->resource;
    }

    /**
     * Sets the name.
     * @param string $resource
     */
    public function setResource($resource) {
        $this->resource = $resource;
    }
    
    /**
     * Sets action.
     * @param \Triangl\Entity\Security\Action $action
     * @return \Triangl\Entity\Security\Firewall this
     */
    public function setAction(Action $action = null) {
        $this->action = $action;
        return $this;
    }
    
    /**
     * Gets action.
     * @return \Triangl\Entity\Security\Action
     */
    public function getAction() {
        return $this->action;
    }
}
