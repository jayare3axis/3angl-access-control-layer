<?php

namespace Triangl\Entity\Security;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\NameTrait;
use Triangl\Entity\AliasTrait;
use Triangl\Entity\Security\UserRoleAssociationTrait;
use Triangl\Entity\Security\RoleFirewallAssociationTrait;

/**
 * Role entity.
 * @Entity @Table(name="roles")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 **/
class Role {
    use PrimaryIdTrait;    
    use NameTrait;
    use AliasTrait;
    use UserRoleAssociationTrait;
    use RoleFirewallAssociationTrait;
    
    /**
     * @OneToMany(targetEntity="\Triangl\Entity\Security\UserRoleAssociation", mappedBy="role")
     */
    private $user_role_associations;
    
    /**
     * @OneToMany(targetEntity="\Triangl\Entity\Security\RoleFirewallAssociation", mappedBy="role")
     */
    private $role_firewall_associations;
    
    /**
     * Default constructor.
     */
    public function __construct() {
        $this->user_role_associations = new ArrayCollection();
        $this->role_firewall_associations = new ArrayCollection();
    }
}
