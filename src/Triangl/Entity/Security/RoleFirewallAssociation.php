<?php

namespace Triangl\Entity\Security;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\Security\Role;
use Triangl\Entity\Security\Firewall;

/**
 * Association between role and firewall.
 * @Entity @Table(name="role_firewalls")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 */
class RoleFirewallAssociation {
    use PrimaryIdTrait;
    
    /**
     * @ManyToOne(targetEntity="Triangl\Entity\Security\Role", inversedBy="role_firewall_associations")
     * @JoinColumn(name="role_id", referencedColumnName="id")
     */
    private $role;
 
    /**
     * @ManyToOne(targetEntity="Triangl\Entity\Security\Firewall", inversedBy="role_firewall_associations")
     * @JoinColumn(name="firewall_id", referencedColumnName="id")
     */
    private $firewall;
    
    /**
     * Set user.
     * @param \Triangl\Entity\Security\Role $role
     * @return \Triangl\Entity\Security\RoleFirewallAssociation this
     */
    public function setRole(Role $role = null) {
        $this->role = $role; 
        return $this;
    }
 
    /**
     * Get role.
     * @return \Triangl\Entity\Security\Role
     */
    public function getRole() {
        return $this->role;
    }
 
    /**
     * Set firewall.
     * @param \Triangl\Entity\Security\Firewall $firewall
     * @return \Triangl\Entity\Security\RoleFirewallAssociation
     */
    public function setFirewall(Firewall $firewall = null) {
        $this->firewall = $firewall; 
        return $this;
    }
 
    /**
     * Get firewall.
     * @return \Triangl\Entity\Security\Firewall
     */
    public function getFirewall() {
        return $this->firewall;
    }
}
