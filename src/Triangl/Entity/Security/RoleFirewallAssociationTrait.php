<?php

namespace Triangl\Entity\Security;

use Triangl\Entity\Security\RoleFirewallAssociation;

/**
 * Entity with property of type role-firewall association
 */
trait RoleFirewallAssociationTrait {    
    /**
     * Add role firewall association.
     * @param \Triangl\Entity\Security\RoleFirewallAssociation $roleFirewallAssociation
     * @return mixed this
     */
    public function addRoleFirewallAssociation(RoleFirewallAssociation $roleFirewallAssociation)
    {
        $this->role_firewall_associations[] = $roleFirewallAssociation; 
        return $this;
    }
 
    /**
     * Remove role firewall association.
     * @param \Triangl\Entity\Security\RoleFirewallAssociation $roleFirewallAssociation
     */
    public function removeRoleFirewallAssociation(RoleFirewallAssociation $roleFirewallAssociation)
    {
        $this->role_firewall_associations->removeElement($roleFirewallAssociation);
    }
     
    /**
     * Gets associations.
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoleFirewallAssociations()
    {
        return $this->role_firewall_associations;
    }
}
