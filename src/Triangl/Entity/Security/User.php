<?php

namespace Triangl\Entity\Security;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\Security\UserDomainAssociationTrait;
use Triangl\Entity\Security\UserRoleAssociationTrait;

/**
 * User entity.
 * @Entity @Table(name="users")
 * @Entity(repositoryClass="Triangl\Entity\Security\UserRepository")
 **/
class User {
    use PrimaryIdTrait;
    use UserDomainAssociationTrait;
    use UserRoleAssociationTrait;
    
    /** @Column(type="string") **/
    private $login;
    
    /** @Column(type="string", nullable=true) **/
    private $password = null;
    
    /**
     * @OneToMany(targetEntity="\Triangl\Entity\Security\UserDomainAssociation", mappedBy="user")
     */
    private $user_domain_associations;
    
    /**
     * @OneToMany(targetEntity="\Triangl\Entity\Security\UserRoleAssociation", mappedBy="user")
     */
    private $user_role_associations;
    
    /**
     * Default constructor.
     */
    public function __construct() {
        $this->user_domain_associations = new ArrayCollection();
        $this->user_role_associations = new ArrayCollection();
    }
        
    /**
     * Gets login.
     * @return string
     */
    public function getLogin() {
        return $this->login;
    }

    /**
     * Sets login.
     * @param string $login
     */
    public function setLogin($login) {
        $this->login = $login;
    }
    
    /**
     * Gets encoded password.
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Sets encoded password.
     * @param string $password
     */
    public function setPassword($password) {
        $this->password = $password;
    }
}
