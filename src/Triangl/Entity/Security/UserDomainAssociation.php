<?php

namespace Triangl\Entity\Security;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\Security\User;
use Triangl\Entity\Security\Domain;

/**
 * Association between user and domain.
 * @Entity @Table(name="user_domains")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 */
class UserDomainAssociation {
    use PrimaryIdTrait;
    
    /**
     * @ManyToOne(targetEntity="Triangl\Entity\Security\User", inversedBy="user_domain_associations")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
 
    /**
     * @ManyToOne(targetEntity="Triangl\Entity\Security\Domain", inversedBy="user_domain_associations")
     * @JoinColumn(name="domain_id", referencedColumnName="id")
     */
    private $domain;
    
    /**
     * Set user.
     * @param \Triangl\Entity\Security\User $user
     * @return \Triangl\Entity\Security\UserDomainAssociation this
     */
    public function setUser(User $user = null) {
        $this->user = $user; 
        return $this;
    }
 
    /**
     * Get user.
     * @return \Triangl\Entity\Security\User
     */
    public function getUser() {
        return $this->user;
    }
 
    /**
     * Set domain.
     * @param \Triangl\Entity\Security\Domain $domain
     * @return \Triangl\Entity\Security\UserDomainAssociation
     */
    public function setDomain(Domain $domain = null) {
        $this->domain = $domain; 
        return $this;
    }
 
    /**
     * Get domain.
     * @return \Triangl\Entity\Security\Domain
     */
    public function getDomain() {
        return $this->domain;
    }
}
