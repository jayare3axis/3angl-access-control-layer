<?php

namespace Triangl\Entity\Security;

use Triangl\Entity\Security\UserDomainAssociation;

/**
 * Entity with property of type user-domain association
 */
trait UserDomainAssociationTrait {    
    /**
     * Add user domain association.
     * @param \Triangl\Entity\Security\UserDomainAssociation $userDomainAssociation
     * @return mixed this
     */
    public function addUserDomainAssociation(UserDomainAssociation $userDomainAssociation)
    {
        $this->user_domain_associations[] = $userDomainAssociation; 
        return $this;
    }
 
    /**
     * Remove user domain association.
     * @param \Triangl\Entity\Security\UserDomainAssociation $userDomainAssociation
     */
    public function removeUserDomainAssociation(UserDomainAssociation $userDomainAssociation)
    {
        $this->user_domain_associations->removeElement($userDomainAssociation);
    }
     
    /**
     * Gets associations.
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserDomainAssociations()
    {
        return $this->user_domain_associations;
    }
}
