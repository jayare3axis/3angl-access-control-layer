<?php

namespace Triangl\Entity\Security;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\User as SymfonyUser;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

use Triangl\Entity\EntityRepository;

/**
 * Repository used by User entity.
 */
class UserRepository extends EntityRepository implements UserProviderInterface {
    /**
     * Implemented.
     */
    public function loadUserByUsername($username) {
        $instance = $this->findOneBy( array("login" => $username) );
        if (!$instance) {
            throw new UsernameNotFoundException( sprintf('Login "%s" does not exist.', $username) );
        }
        
        $password = $instance->getPassword();
        
        // Handle roles.
        $roles = array();
        foreach ( $instance->getUserRoleAssociations() as $association ) {
            $roles[] = $association->getRole()->getAlias();
        }
        
        return new SymfonyUser( $instance->getLogin(), $password, $roles );
    }

    /**
     * Implemented.
     */
    public function refreshUser(UserInterface $user) {
        if (!$user instanceof SymfonyUser) {
            throw new UnsupportedUserException( sprintf('Instances of "%s" are not supported.', get_class($user)) );
        }
        return $this->loadUserByUsername( $user->getUsername() );
    }

    /**
     * Implemented.
     */
    public function supportsClass($class) {
        return $class === 'Symfony\Component\Security\Core\User\User';
    }
}
