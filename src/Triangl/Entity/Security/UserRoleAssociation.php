<?php

namespace Triangl\Entity\Security;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\Security\User;
use Triangl\Entity\Security\Role;

/**
 * Association between user and role.
 * @Entity @Table(name="user_roles")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 */
class UserRoleAssociation {
    use PrimaryIdTrait;
    
    /**
     * @ManyToOne(targetEntity="Triangl\Entity\Security\User", inversedBy="user_role_associations")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
 
    /**
     * @ManyToOne(targetEntity="Triangl\Entity\Security\Role", inversedBy="user_role_associations")
     * @JoinColumn(name="role_id", referencedColumnName="id")
     */
    private $role;
    
    /**
     * Set user.
     * @param \Triangl\Entity\Security\User $user
     * @return \Triangl\Entity\Security\UserRoleAssociation this
     */
    public function setUser(User $user = null) {
        $this->user = $user; 
        return $this;
    }
 
    /**
     * Get user.
     * @return \Triangl\Entity\Security\User
     */
    public function getUser() {
        return $this->user;
    }
 
    /**
     * Set role.
     * @param \Triangl\Entity\Security\Role $role
     * @return \Triangl\Entity\Security\UserRoleAssociation
     */
    public function setRole(Role $role = null) {
        $this->role = $role; 
        return $this;
    }
 
    /**
     * Get role.
     * @return \Triangl\Entity\Security\Role
     */
    public function getRole() {
        return $this->role;
    }
}
