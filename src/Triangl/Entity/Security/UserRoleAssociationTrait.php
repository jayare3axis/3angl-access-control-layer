<?php

namespace Triangl\Entity\Security;

use Triangl\Entity\Security\UserRoleAssociation;

/**
 * Entity with property of type user-role association
 */
trait UserRoleAssociationTrait {    
    /**
     * Add user role association.
     * @param \Triangl\Entity\Security\UserRoleAssociation $userRoleAssociation
     * @return mixed this
     */
    public function addUserRoleAssociation(UserRoleAssociation $userRoleAssociation)
    {
        $this->user_role_associations[] = $userRoleAssociation; 
        return $this;
    }
 
    /**
     * Remove user role association.
     * @param \Triangl\Entity\Security\UserRoleAssociation $userRoleAssociation
     */
    public function removeUserRoleAssociation(UserRoleAssociation $userRoleAssociation)
    {
        $this->user_role_associations->removeElement($userRoleAssociation);
    }
     
    /**
     * Gets associations.
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserRoleAssociations()
    {
        return $this->user_role_associations;
    }
}
