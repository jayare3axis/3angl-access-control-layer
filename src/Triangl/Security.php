<?php

namespace Triangl;

use Symfony\Component\HttpFoundation\Request;

use Silex\ServiceProviderInterface;
use Silex\Provider\SecurityServiceProvider;

/*
 * Triangl security module.
 */
class Security implements ServiceProviderInterface {
    private $app;
    
    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app) {
    }

    /**
     * Implemented.
     */
    public function register(\Silex\Application $app) {        
        // Default configurations.
        $app["security.values"] = array(
            "security.firewalls" => array()
        );
        
        // Register entities.
        $app['triangl.entities']->addNamespace( array(
            'type' => 'annotation',
            'path' => __DIR__ . '/Entity/Security',
            'namespace' => 'Triangl\Entity\Security'
        ) );
        
        // Register services.
        $app->register( new SecurityServiceProvider(), $app["security.values"] );
        $app['triangl.security'] = $app->share( function ($app) {
            return new SecurityHelper($app);
        } );
        
        // If testing make sure there is atleast one firewall.
        if ($app["test"]) {
            $app['triangl.security']->addFirewall( array(
                'test' => array(
                    'pattern' => '^/test',
                    'http' => true
                )   
            ) );
        }
        
        // Middleware.
        $app->before(function (Request $request, \Silex\Application $app) {
            // Pass user instance to container.
            $user = $app->user();
            if ($user) {
                $instance = $app['db.orm.em']->getRepository('Triangl\Entity\Security\User')
                    ->findOneBy( array( 'login' => $user->getUsername() ) );
                $app["security.user.instance"] = $instance;
            }
            else {
                $app["security.user.instance"] = null;
            }
            
            // Make sure user have access for given property.
            $this->app = $app; // Just a little hack so it will be visible inside listener :(
            $app['dispatcher']->addListener('entities.access.property', function(AccessPropertyEvent $event) {
                $action = $this->app['triangl.security']->getActionByAlias('view');
                $event->setAllowed( 
                    $this->app['triangl.security']->isUserAllowed($this->app["security.user.instance"], $event->getClassName() . ":" . $event->getProperty(), $action ) 
                );
            });
        });
    }
}
