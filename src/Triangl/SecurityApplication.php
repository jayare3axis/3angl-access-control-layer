<?php

namespace Triangl;

use Silex\Application\SecurityTrait;

/**
 * Application with security module.
 */
class SecurityApplication extends EntitySystemApplication {
    use SecurityTrait;
    
    /**
     * Overriden.
     */
    protected function init() {
        parent::init();
        
        $this->register( new Security() );
    }
}
