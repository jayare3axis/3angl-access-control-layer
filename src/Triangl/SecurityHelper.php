<?php

namespace Triangl;

use Triangl\Entity\Security\User;
use Triangl\Entity\Security\Action;

/**
 * Helper routines for security module.
 */
class SecurityHelper {
    private $app;
    
    /**
     * Default constructor.
     */
    public function __construct(Application $app) {
        $this->app = $app;
    }
    
    /**
     * Adds a firewall.
     * @param array $firewall a firewall setting following Symfony2 Security spec.
     */
    public function addFirewall(array $firewall) {
        $this->app["security.firewalls"] = array_merge($this->app["security.firewalls"], $firewall);
    }
    
    /**
     * Returns instance of action by given alias.
     * @param string $alias
     * @return \Triangl\Entity\Security\Action
     * @throws \InvalidArgumentException
     */
    public function getActionByAlias($alias) {
        $action = $this->app['db.orm.em']->getRepository('Triangl\Entity\Security\Action')
            ->findOneBy( array( "alias" => "view" ) );
        if (!$action) {
            throw new \InvalidArgumentException("Action with alias $alias was not found.");
        }
        return $action;
    }
    
    /**
     * Check if user is allow to perform action upon given resource.
     * @param \Triangl\Entity\Security\User|null $user
     * @param string $resource
     * @param \Triangl\Entity\Security\Action $action
     * @return boolean
     */
    public function isUserAllowed($user, $resource, Action $action) {        
        $em = $this->app["db.orm.em"];
        $result = true;
        
        // If action is protected user authentication is required.
        if ( $action->isProtected() && $user == null ) {
            $result = false;
        }
        
        $help = explode(':', $resource);
        $entity = $help[0];
        
        // Check if entity is firewalled.
        $firewall = $em->getRepository("Triangl\Entity\Security\Firewall")
            ->findOneBy( array("resource" => $entity, "action" => $action) );
        if ( $firewall != null && !$this->isUserAllowedAccess($user, $entity, $action) ) {
            $result = false;
        }
        
        // Check if entity and a property is firewalled.
        if ( count($help) > 1 ) {
            $firewall = $em->getRepository("Triangl\Entity\Security\Firewall")
                ->findOneBy( array("resource" => $resource, "action" => $action) );
            if ( $firewall != null && !$this->isUserAllowedAccess($user, $resource, $action) ) {
                $result = false;
            }
        }
                        
        return $result;
    }
    
    /**
     * Check if user authenticated and has allowed to access given resource
     * with target action.
     * @param \Triangl\Entity\Security\User|null $user
     * @param string $resource
     * @param \Triangl\Entity\Security\Action $action
     * @return boolean
     */
    private function isUserAllowedAccess($user, $resource, Action $action) {
        if ($user == null) {
            return false;
        }
        
        foreach ($user->getUserRoleAssociations() as $a) {
            $role = $a->getRole();
            foreach ($role->getRoleFirewallAssociations() as $aa) {
                $firewall = $aa->getFirewall();
                if ( $action->getAlias() == $firewall->getAction()->getAlias()
                     && $resource == $firewall->getResource() ) {
                    return true;
                }
            }
        }
        return false;
    }
}
