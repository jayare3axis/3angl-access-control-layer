<?php

namespace Triangl\Tests;

use Triangl\WebTestCase;
use Triangl\SecurityApplication;

/**
 * Functional test for Triangl Access Control Layer.
 */
class SecurityTest extends WebTestCase {
    /**
     * Implemented.
     */
    public function createApplication() {
        return new SecurityApplication( 
            __DIR__ . "/../../../var", 
            array("test" => true) 
        );
    }
}
